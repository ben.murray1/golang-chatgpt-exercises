In my efforts to become more familiar and capable with Go, I asked ChatGPT to devise
exercises of increasing difficulty to challenge my knowledge and promote improvement.

I gave it this exact prompt:

*I would lke you to help me devise some exercises for practicing specific concepts of the Go language. I will provide you with the topic and a difficulty rating from 1-5 (5 is hardest). You will then come up with a challenge designed to test my knowledge of the concept and I will then give you my coded solution. You can then provide any constructive feedback.*

Following this, I fed it simple 'Concept - Difficulty' inputs to which it devised challenges

Naming conventions for these challenges in this folder are, similarly, 'concept-difficulty', with each challenge
occupying it's own folder and housing a 'main.go' file.

