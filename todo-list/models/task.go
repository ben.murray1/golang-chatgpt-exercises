package models

import "fmt"

type Task struct {
	ID          int
	Title       string
	Description string
	Completed   bool
}

type Tasks struct {
	Tasks []Task
}

func (t Tasks) ViewTasks() {
	_, err := fmt.Println(t.Tasks)
	if err != nil {
		fmt.Println(err)
	}
}

func (t *Tasks) MarkTaskAsCompleted(targID int) {
	for i := range t.Tasks {
		if t.Tasks[i].ID == targID {
			t.Tasks[i].Completed = !t.Tasks[i].Completed
			break
		}
	}
}

func (t *Tasks) AddTask(id int, title, description string, completed bool) {
	t.Tasks = append(t.Tasks, Task{
		ID:          id,
		Title:       title,
		Description: description,
		Completed:   completed,
	})
}

func (t *Tasks) RemoveTask(id int) {
	var deleteIndex int

	for i, v := range t.Tasks {
		if v.ID == id {
			deleteIndex = i
			break
		}
	}

	if deleteIndex == 0 && t.Tasks[0].ID != id {
		fmt.Println("ID provided does not match any tasks")
		return
	}

	t.Tasks = append(t.Tasks[:deleteIndex], t.Tasks[deleteIndex+1:]...)
}
