package main

import (
	"bufio"
	"fmt"
	"os"
	"todo-list/models"
)

func main() {
	taskList := models.Tasks{
		Tasks: []models.Task{
			{ID: 1,
				Title:       "Feed dog",
				Description: "Benjy is hungry",
				Completed:   false},
			{ID: 2,
				Title:       "Go gym",
				Description: "Push day today",
				Completed:   false},
		},
	}
	var selectedTask string
	reader := bufio.NewReader(os.Stdin)
	exit := false
	for !exit {
		fmt.Println("---------------------\nWhat task would you like to perform?\nView Tasks(v)\tAdd Task(a)\tRemove Task(r)\tMark Task As Completed(m)\tExit(x)")
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
		}
		selectedTask = input[:len(input)-1]
		if selectedTask != "v" && selectedTask != "a" && selectedTask != "r" && selectedTask != "m" && selectedTask != "x" {
			fmt.Println("Invalid input, please try again")
			selectedTask = ""
		}

		if selectedTask == "v" {
			taskList.ViewTasks()
			selectedTask = ""
		}
		if selectedTask == "a" {
			var title, description string
			fmt.Println("Please provide the details of your task...\nTitle: ")
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
			}
			title = input[:len(input)-1]
			fmt.Println("Description: ")
			input, err = reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
			}
			description = input[:len(input)-1]
			taskList.AddTask(len(taskList.Tasks)+1, title, description, false)
			//test,error
			fmt.Printf("-------------------\nAdded %v to task list\n", title)
			selectedTask = ""
		}
		if selectedTask == "r" {
			var targID string
			fmt.Println("Please provide the ID of the task you want to remove...\tID: ")
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
			}
			targID = input[:len(input)-1]
			taskList.RemoveTask(int(targID[0] - '0'))
			//test,error
			fmt.Println("\nTask removal successful")
			selectedTask = ""
		}
		if selectedTask == "m" {
			var targID string
			fmt.Println("Please provide the ID of the task you want to update...\tID: ")
			input, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println(err)
			}
			targID = input[:len(input)-1]
			taskList.MarkTaskAsCompleted(int(targID[0] - '0'))
			//test,error
			fmt.Println("\nTask status update successful")
			selectedTask = ""
		}
		if selectedTask == "x" {
			fmt.Println("See ya!")
			break
		}
	}
}

//include error handling and tests
//more robust unique ID generation
