package main

import "fmt"

type Employee struct {
	ID        int
	FirstName string
	LastName  string
	Position  string
	Salary    float64
}

type EmployeeManagement struct {
	Employees []Employee
}

func (em *EmployeeManagement) AddEmployee(newEmployee Employee) {
	em.Employees = append(em.Employees, newEmployee)
}

func (em *EmployeeManagement) ListEmployees() {
	for _, employee := range em.Employees {
		fmt.Printf("ID: %d, Name: %s %s, Position: %s, Salary: %.2f\n",
			employee.ID, employee.FirstName, employee.LastName, employee.Position, employee.Salary)
	}
}

func (em *EmployeeManagement) GetEmployeeByID(id int) Employee {
	for _, employee := range em.Employees {
		if employee.ID == id {
			return employee
		}
	}
	return Employee{}
}

func main() {
	employeeSystem := EmployeeManagement{
		Employees: []Employee{
			{1, "John", "Smith", "Cleaner", 20000},
			{2, "Sarah", "Duggan", "CEO", 60000},
		},
	}

	newEmp := Employee{
		ID:        3,
		FirstName: "Alice",
		LastName:  "Johnson",
		Position:  "Manager",
		Salary:    45000,
	}
	employeeSystem.AddEmployee(newEmp)

	fmt.Println("List of employees:")
	employeeSystem.ListEmployees()

	fmt.Println("\nEmployee by ID:")
	employeeByID := employeeSystem.GetEmployeeByID(2)
	if employeeByID.ID != 0 {
		fmt.Printf("ID: %d, Name: %s %s, Position: %s, Salary: %.2f\n",
			employeeByID.ID, employeeByID.FirstName, employeeByID.LastName, employeeByID.Position, employeeByID.Salary)
	} else {
		fmt.Println("Employee not found.")
	}
}
