package main

import "fmt"

func doubleSlice(slicePtr *[]int) {
	dupeSlice := *slicePtr
	for i, v := range dupeSlice {
		dupeSlice[i] = v * 2
	}
	*slicePtr = dupeSlice
}

func main() {
	numbers := []int{1, 2, 3, 4, 5}

	fmt.Println("Original slice:", numbers)
	doubleSlice(&numbers)
	fmt.Println("Doubled slice:", numbers)
}

//Write the doubleSlice function to double the values of the elements within the slice using pointers.
