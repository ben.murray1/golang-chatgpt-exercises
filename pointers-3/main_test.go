package main

import (
	"reflect"
	"testing"
)

func TestDoubleSlice(t *testing.T) {
	numbers := []int{1, 2, 3, 4, 5}
	expected := []int{2, 4, 6, 8, 10}

	doubleSlice(&numbers)

	if !reflect.DeepEqual(numbers, expected) {
		t.Errorf("Expected %v, but got %v", expected, numbers)
	}
}

//reflect.DeepEqual: Function provided by the reflect package in Go. It's used to compare two values for deep equality. Deep equality means that not only the top-level values are equal, but also their nested elements (if they are slices, maps, structs, etc.) are equal.
