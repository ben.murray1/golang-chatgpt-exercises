package main

import "fmt"

type BankAccount struct {
	AccountNumber string
	AccountHolder string
	Balance       float64
}

func NewAccount(accNum string, accHolder string, balance float64) BankAccount {
	return BankAccount{
		AccountNumber: accNum,
		AccountHolder: accHolder,
		Balance:       balance,
	}
}

func (ba *BankAccount) Deposit(amount float64) {
	ba.Balance += amount
}

func (ba *BankAccount) Withdraw(amount float64) {
	ba.Balance -= amount
}

func main() {
	account := NewAccount("247", "Bob Jenkins", 1234.56)
	fmt.Println(account)

	account.Withdraw(50.64)
	fmt.Println("After withdrawal:", account)

	account.Deposit(100.00)
	fmt.Println("After deposit:", account)
}
