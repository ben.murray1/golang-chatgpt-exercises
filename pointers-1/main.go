package main

import "fmt"

func swapValues(a, b *int) {
	newAVal := *b
	newBVal := *a
	*a = newAVal
	*b = newBVal
	//SUCCESS ----------------- ChatGPT suggests: *a, *b = *b, *a
}

func main() {
	x := 5
	y := 10

	fmt.Println("Before swapping:")
	fmt.Println("x:", x)
	fmt.Println("y:", y)

	swapValues(&x, &y)

	fmt.Println("After swapping:")
	fmt.Println("x:", x)
	fmt.Println("y:", y)
}

/*
Create a function in Go that swaps the values of two integer variables using pointers.
Your function should take two pointers to integers as arguments and modify the values they point to.
*/
