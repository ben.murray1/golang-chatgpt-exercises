package main

import (
	"fmt"
)

type TemperatureError struct {
	Msg string
}

func (e TemperatureError) Error() string {
	return e.Msg
}

func FahrenheitToCelsius(fahrenheit float64) (float64, error) {
	if fahrenheit < -459.67 {
		return 0, TemperatureError{Msg: "Temperature is below absolute zero"}
	} else {
		return (fahrenheit - 32) * 5 / 9, nil
	}
}

func main() {
	var fahrenheit float64
	fmt.Print("Enter temperature in Fahrenheit: ")
	_, err := fmt.Scanf("%f", &fahrenheit)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	celsius, err := FahrenheitToCelsius(fahrenheit)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Printf("%.2f°F is equal to %.2f°C\n", fahrenheit, celsius)
}
